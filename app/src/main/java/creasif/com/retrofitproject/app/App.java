package creasif.com.retrofitproject.app;

import android.app.Application;

/**
 * Created by j3p0n on 3/29/2016.
 */
public class App extends Application
{
    private static RestClient restClient;

    @Override
    public void onCreate()
    {
        super.onCreate();

        restClient = new RestClient();
    }

    public static RestClient getRestClient()
    {
        return restClient;
    }
}
