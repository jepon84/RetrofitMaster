package creasif.com.retrofitproject.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by j3p0n on 3/29/2016.
 */
public class Content {

    @SerializedName("content_id")
    private String contentId;
    @SerializedName("section_id")
    private String sectionId;
    @SerializedName("cat_id")
    private String catId;
    @SerializedName("user_id")
    private String userId;
    @SerializedName("content_name")
    private String contentName;
    @SerializedName("content_alias")
    private String contentAlias;
    @SerializedName("content_desc")
    private String contentDesc;
    @SerializedName("content_tags")
    private String contentTags;
    @SerializedName("content_hits")
    private String contentHits;
    @SerializedName("content_status")
    private String contentStatus;
    @SerializedName("content_create_date")
    private String contentCreateDate;
    @SerializedName("content_publish_date")
    private String contentPublishDate;
    @SerializedName("content_image")
    private String contentImage;
    @SerializedName("section_name")
    private String sectionName;

    public String getContentId() {
        return contentId;
    }

    public String getSectionId() {
        return sectionId;
    }

    public String getCatId() {
        return catId;
    }

    public String getUserId() {
        return userId;
    }

    public String getContentName() {
        return contentName;
    }

    public String getContentAlias() {
        return contentAlias;
    }

    public String getContentDesc() {
        return contentDesc;
    }

    public String getContentTags() {
        return contentTags;
    }

    public String getContentHits() {
        return contentHits;
    }

    public String getContentStatus() {
        return contentStatus;
    }

    public String getContentCreateDate() {
        return contentCreateDate;
    }

    public String getContentPublishDate() {
        return contentPublishDate;
    }

    public String getContentImage() {
        return contentImage;
    }

    public String getSectionName() {
        return sectionName;
    }
}