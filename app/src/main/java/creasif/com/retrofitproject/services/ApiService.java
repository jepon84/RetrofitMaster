package creasif.com.retrofitproject.services;

import java.util.List;
import java.util.Map;

import creasif.com.retrofitproject.models.Content;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by j3p0n on 3/29/2016.
 */
public interface ApiService {

    @FormUrlEncoded
    @POST("APIcontent.php")
    Observable<List<Content>> requestContent(
            @FieldMap Map<String,String> params
    );
}
